use std::sync::{Arc, Mutex};
use std::{ffi::c_void, mem, ptr, str};

pub struct Event {}
impl Event {
    pub fn register_default_impl() -> Result<(), Error> {
        let ret = unsafe { sys::virEventRegisterDefaultImpl() };
        if ret == -1 {
            return Err(Error::last_error());
        }
        Ok(())
    }

    pub fn run_default_impl() -> Result<(), Error> {
        let ret = unsafe { sys::virEventRunDefaultImpl() };
        if ret == -1 {
            return Err(Error::last_error());
        }
        Ok(())
    }


    #[allow(clippy::not_unsafe_ptr_arg_deref)]
    pub fn domain_event_register_any<T>(
        conn: &Connect,
        domain: &Domain,
        event: u32,
        callback: *const (),
        userdata: Option<Arc<Mutex<T>>>,
    ) -> Result<i32, Error> {
        let ptr: *mut c_void = unsafe { mem::transmute(userdata) };
        unsafe extern "C" fn empty_callback(_: *mut c_void) {}

        let callback: sys::virConnectDomainEventGenericCallback =
            unsafe { mem::transmute(callback) };
        let ret = unsafe {
            sys::virConnectDomainEventRegisterAny(
                conn.as_ptr(),
                domain.as_ptr(),
                event as i32,
                callback,
                ptr,
                Some(empty_callback),
            )
        };

        if ret == -1 {
            return Err(Error::last_error());
        }
        Ok(ret)
    }

    pub fn domain_event_deregister_any(conn: &Connect, callback_id: u32) -> Result<(), Error> {
        let ret =
            unsafe { sys::virConnectDomainEventDeregisterAny(conn.as_ptr(), callback_id as i32) };

        if ret == -1 {
            return Err(Error::last_error());
        }
        Ok(())
    }
}
